from aiogram import Router, F
from aiogram.filters import Command, CommandStart, StateFilter, Text
from aiogram.fsm.state import default_state
from aiogram.filters.state import State, StatesGroup
from aiogram.fsm.context import FSMContext
from aiogram.types import (Message, InlineKeyboardButton,
                           InlineKeyboardMarkup, CallbackQuery)
from lexicon.lexicon import LEXICON
from images.images import pink

user_router: Router = Router()


# Стартовая команда
@user_router.message(CommandStart(), StateFilter(default_state))
async def process_start_command(message: Message):
    await message.answer(LEXICON['/start'])


# Для машины состояния
class FSMFillInfo(StatesGroup):
    fill_user_name = State()
    fill_bunny_name = State()
    fill_pronoun = State()


# Этот хэндлер будет срабатывать на команду /fillinfo
# и переводить бота в состояние ожидания ввода имени
@user_router.message(Command(commands='fillinfo'), StateFilter(default_state))
async def process_fillinfo_command(message: Message, state: FSMContext):
    await message.answer(LEXICON['/fillinfo'])
    await state.set_state(FSMFillInfo.fill_user_name)


# Этот хэндлер срабатывает на корректно введёное имя/псевдоним
# и переводит бота в состояние ожидания ввода имени кролика
@user_router.message(StateFilter(FSMFillInfo.fill_user_name), F.text.isalpha())
async def process_fill_user_name_sent(message: Message, state: FSMContext):
    await state.update_data(user_name=message.text)
    await message.answer(text=f'Приятно познакомиться, {message.text}!'
                              '\n\nА теперь придумай имя кролику')
    await state.set_state(FSMFillInfo.fill_bunny_name)


# Этот хэндлер срабатывает на некорректный ввод имени/псевдонима
@user_router.message(StateFilter(FSMFillInfo.fill_user_name))
async def warning_not_user_name(message: Message):
    await message.answer(text='Твоё имя или псевдоним могут состоять только'
                              'из букв латиницы или кириллицы\n\n'
                              'Если ты хочешь начать сначала - '
                              'отправь команду /cancel')


# Этот хэндлер срабатывает на корректно введёное имя кролика
# и переводит бота в состояние ожидания выбора местоимения
@user_router.message(StateFilter(FSMFillInfo.fill_bunny_name),
                     F.text.isalpha())
async def process_bunny_user_name_sent(message: Message, state: FSMContext):
    await state.update_data(bunny_name=message.text)
    he1_button = InlineKeyboardButton(text='Он',
                                      callback_data='he1')
    she1_button = InlineKeyboardButton(text='Она',
                                       callback_data='she1')
    they1_button = InlineKeyboardButton(text='Они',
                                        callback_data='they1')
    keyboard: list[list[InlineKeyboardButton]] = [[he1_button], [she1_button],
                                                  [they1_button]]
    markup = InlineKeyboardMarkup(inline_keyboard=keyboard)
    await message.answer(text='Отлично! Остался последний вопрос.\n\n'
                              'Укажи предпочитаемое местоимение',
                         reply_markup=markup)
    await state.set_state(FSMFillInfo.fill_pronoun)


# Этот хэндлер срабатывает на некорректный ввод имени кролика
@user_router.message(StateFilter(FSMFillInfo.fill_bunny_name))
async def warning_not_bunny_name(message: Message):
    await message.answer(text='Имя кролика может состоять только'
                              'из букв латиницы или кириллицы\n\n'
                              'Если ты хочешь начать сначала - '
                              'отправь команду /cancel')


# Этот хэндлер будет срабатывать на выбор местоимения
# и выводить из машины состояний
@user_router.callback_query(StateFilter(FSMFillInfo.fill_pronoun),
                            Text(text=['he1', 'she1', 'they1']))
async def process_1st_pronoun_press(callback: CallbackQuery,
                                    state: FSMContext):
    await state.update_data(first_pronoun=callback.data)
    text = 'Спасибо! Я узнал всю необходимую информацию. Взгляните на ' \
           'вашего нового друга!\n\nЧтобы узнать, как взаимодействовать ' \
           'с ботом, выберите в меню команду /help'
    await callback.message.answer_photo(pink, caption=text)
    await state.clear()
