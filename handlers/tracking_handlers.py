from aiogram import Router
from aiogram.filters import Command
from aiogram.types import (Message, InlineKeyboardButton,
                           InlineKeyboardMarkup, CallbackQuery)
from lexicon.lexicon import LEXICON
from images.images import *
from keyboard.skin_keyboard import *

tracking_router: Router = Router()


# Класс, отвечающий за накопление звёзд и повышение уровня
class Pet:
    def __init__(self):
        self.stars = 0
        self.level = 1

    async def add_stars(self, count, message: Message):
        self.stars += count
        while self.stars >= self.level * 50:
            self.level += 1
        if self.level > 1:
            await message.answer(f'Ваш кролик достиг {self.level} уровня! '
            'Теперь вы можете обновить его внешний вид. Для этого '
            'нажмите команду /change_skin')

    def get_level(self):
        return self.level

    def get_stars(self):
        return self.stars


pet = Pet()


# Хэндлер для отметки действий за день
@tracking_router.message(Command(commands='track'))
async def process_track_command(message: Message):
    await message.answer(LEXICON['/track'])


current_skin = pink


# Хэндлер для вывода информации о кролике
@tracking_router.message(Command(commands='stats'))
async def show_stats(message: Message):
    stars = pet.get_stars()
    level = pet.get_level()
    text = f'Уровень: {level}\nНакоплено звёзд: {stars}'
    await message.answer_photo(current_skin, caption=text)


# Хэндлер для изменения скина
@tracking_router.message(Command(commands='change_skin'))
async def process_change_skin(message: Message):
    keyboard: list[list[InlineKeyboardButton]] = [
        [pink_button, yellow_button, purple_button, blue_button,
        lying_pink_button, lying_yellow_button], 
        [lying_purple_button, lying_blue_button, pink_ears_button,
         yellow_ears_button, purple_ears_button, blue_ears_button],
        [pink_flower_button, pink_lamp_button, pink_candle_button,
         yellow_flower_button, yellow_lamp_button, yellow_candle_button],
        [purple_flower_button, purple_lamp_button, purple_candle_button,
         blue_flower_button, blue_lamp_button, blue_candle_button],
        [lying_pink_flower_button, lying_pink_lamp_button,
         lying_pink_candle_button, lying_yellow_flower_button,
         lying_yellow_lamp_button, lying_yellow_candle_button],
        [lying_purple_flower_button, lying_purple_lamp_button,
         lying_purple_candle_button, lying_blue_flower_button,
         lying_blue_lamp_button, lying_blue_lamp_button],
        [pink_ears_flower_button, pink_ears_lamp_button,
         pink_ears_candle_button, yellow_ears_flower_button,
         yellow_ears_lamp_button, yellow_ears_candle_button],
        [purple_ears_flower_button, purple_ears_lamp_button,
         purple_ears_candle_button, blue_ears_flower_button,
         blue_ears_lamp_button, blue_ears_candle_button]
        ]
    markup = InlineKeyboardMarkup(inline_keyboard=keyboard)
    await message.answer(LEXICON['/change_skin'], reply_markup=markup)


# Словари для кнопок
skins1 = {
    'pink': pink,
    'yellow': yellow,
    'purple': purple,
    'blue': blue,
}

skins2 = {
    'lying_pink': lying_pink,
    'lying_yellow': lying_yellow,
    'lying_purple': lying_purple,
    'lying_blue': lying_blue,
    'pink_ears': pink_ears,
    'yellow_ears': yellow_ears,
    'purple_ears': purple_ears,
    'blue_ears': blue_ears
}

skins3 = {
    'pink_flower': pink_flower,
    'yellow_flower': yellow_flower,
    'purple_flower': purple_flower,
    'blue_flower': blue_flower,
    'lying_pink_flower': lying_pink_flower,
    'lying_yellow_flower': lying_yellow_flower,
    'lying_purple_flower': lying_purple_flower,
    'lying_blue_flower': lying_blue_flower,
    'pink_ears_flower': pink_ears_flower,
    'yellow_ears_flower': yellow_ears_flower,
    'purple_ears_flower': purple_ears_flower,
    'blue_ears_flower': blue_ears_flower,
    'pink_lamp': pink_lamp,
    'yellow_lamp': yellow_lamp,
    'purple_lamp': purple_lamp,
    'blue_lamp': blue_lamp,
    'lying_pink_lamp': lying_pink_lamp,
    'lying_yellow_lamp': lying_yellow_lamp,
    'lying_purple_lamp': lying_purple_lamp,
    'lying_blue_lamp': lying_blue_lamp,
    'pink_ears_lamp': pink_ears_lamp,
    'yellow_ears_lamp': yellow_ears_lamp,
    'purple_ears_lamp': purple_ears_lamp,
    'blue_ears_lamp': blue_ears_lamp,
    'pink_candle': pink_candle,
    'yellow_candle': yellow_candle,
    'purple_candle': purple_candle,
    'blue_candle': blue_candle,
    'lying_pink_candle': lying_pink_candle,
    'lying_yellow_candle': lying_yellow_candle,
    'lying_purple_candle': lying_purple_candle,
    'lying_blue_candle': lying_blue_candle,
    'pink_ears_candle': pink_ears_candle,
    'yellow_ears_candle': yellow_ears_candle,
    'purple_ears_candle': purple_ears_candle,
    'blue_ears_candle': blue_ears_candle
}

valid_data = list(skins1.keys()) + list(skins2.keys()) + list(skins3.keys())


# Здесь расписаны условия, по которым обновляется скин
@tracking_router.callback_query(lambda c: c.data in valid_data)
async def process_change_skin_callback(callback_query: CallbackQuery):
    global current_skin
    answer = callback_query.data
    message = callback_query.message
    stars = pet.get_stars()
    level = pet.get_level()
    text = f'Ну что за красавец!\n\nУровень: {level}\nНакоплено звёзд: {stars}'
    if level == 1:
        await message.answer(text='Извините, но этот скин вам '
                                      'пока что недоступен')
    await message.delete()
    
    if level == 2:
        if answer in skins1:
            current_skin = skins1[answer]
            await message.answer_photo(current_skin, caption=text)
        else:
            await message.answer(text='Извините, но этот скин вам '
                                      'пока что недоступен')
    await message.delete()

    if level == 3:
        if answer in skins1:
            current_skin = skins1[answer]
            await message.answer_photo(current_skin, caption=text)
        if answer in skins2:
            current_skin = skins2[answer]
            await message.answer_photo(current_skin, caption=text)
        else:
            await message.answer(text='Извините, но этот скин вам '
                                      'пока что недоступен')
    await message.delete()

    if level == 4:
        if answer in skins1:
            current_skin = skins1[answer]
            await message.answer_photo(current_skin, caption=text)
        if answer in skins2:
            current_skin = skins2[answer]
            await message.answer_photo(current_skin, caption=text)
        if answer in skins3:
            current_skin = skins3[answer]
            await message.answer_photo(current_skin, caption=text)
    await message.delete()


# Хэндлер для отметки сна
@tracking_router.message(Command(commands='sleep'))
async def process_sleep_command(message: Message):
    five_button = InlineKeyboardButton(text='меньше 5 часов',
                                       callback_data='five')
    five_seven_button = InlineKeyboardButton(text='5-7 часов',
                                             callback_data='five_seven')
    seven_button = InlineKeyboardButton(text='7 и более часов',
                                        callback_data='seven')
    keyboard: list[list[InlineKeyboardButton]] = [[five_button,
                                                   five_seven_button],
                                                  [seven_button]]
    markup = InlineKeyboardMarkup(inline_keyboard=keyboard)
    await message.answer(LEXICON['/sleep'], reply_markup=markup)


@tracking_router.callback_query(lambda c: c.data in ['five', 'five_seven',
                                                     'seven'])
async def process_sleep_callback(callback_query: CallbackQuery):
    answer = callback_query.data
    message = callback_query.message
    if answer == 'five':
        await message.answer('Вы ни заработали ни одной звездочки счастья\n\n'
                             'Хороший сон — основополагающая вещь '
                             'для поддержания ментального здоровья. '
                             'Не обижай себя и свой организм, '
                             'отдыхай ночью!')
        await pet.add_stars(0, message)
    elif answer == 'five_seven':
        await message.answer('Вы заработали 1 супер звездочку счастья')
        await pet.add_stars(1, message)
    elif answer == 'seven':
        await message.answer('Вы заработали 2 супер звездочки счастья')
        await pet.add_stars(2, message)
    await message.delete()


# Хэндлер для отметки питания
@tracking_router.message(Command(commands='food'))
async def process_food_command(message: Message):
    three_button = InlineKeyboardButton(text='3 раза',
                                        callback_data='three')
    two_button = InlineKeyboardButton(text='3 плохих или 2 хороших раза',
                                      callback_data='two')
    one_button = InlineKeyboardButton(text='1 раз или совсем 0',
                                      callback_data='one')
    keyboard: list[list[InlineKeyboardButton]] = [[one_button, two_button],
                                                  [three_button]]
    markup = InlineKeyboardMarkup(inline_keyboard=keyboard)
    await message.answer(LEXICON['/food'], reply_markup=markup)


@tracking_router.callback_query(lambda c: c.data in ['three', 'two', 'one'])
async def process_food_callback(callback_query: CallbackQuery):
    answer = callback_query.data
    message = callback_query.message
    if answer == 'three':
        await message.answer('Вы заработали 2 супер звездочки счастья')
        await pet.add_stars(2, message)
    elif answer == 'two':
        await message.answer('Вы заработали 1 супер звездочку счастья')
        await pet.add_stars(1, message)
    elif answer == 'one':
        await message.answer('Вы не заработали ни одной звездочки счастья\n\n'
                             'Помни, что еда необходима твоему организму, '
                             'чтобы обнимать друзей и думать крутые мысли')
        await pet.add_stars(0, message)
    await message.delete()


# Хэндлер для отметки воды
@tracking_router.message(Command(commands='water'))
async def process_water_command(message: Message):
    two_button = InlineKeyboardButton(text='1,5 литра или больше',
                                      callback_data='two')
    one_button = InlineKeyboardButton(text='1-1,5 литра',
                                      callback_data='one')
    zero_button = InlineKeyboardButton(text='менее 1 литра',
                                       callback_data='zero')
    keyboard: list[list[InlineKeyboardButton]] = [[zero_button, one_button],
                                                  [two_button]]
    markup = InlineKeyboardMarkup(inline_keyboard=keyboard)
    await message.answer(LEXICON['/water'], reply_markup=markup)


@tracking_router.callback_query(lambda c: c.data in ['two', 'one', 'zero'])
async def process_water_callback(callback_query: CallbackQuery):
    answer = callback_query.data
    message = callback_query.message
    if answer == 'two':
        await message.answer('Вы заработали 2 супер звездочки счастья')
        await pet.add_stars(2, message)
    elif answer == 'one':
        await message.answer('Вы заработали 1 супер звездочку счастья')
        await pet.add_stars(1, message)
    elif answer == 'zero':
        await message.answer('Вы не заработали ни одной звездочки счастья.\n\n'
                             'ты как цветочек, тебе тоже нужна чистая '
                             'водичка, чтобы выжить')
        await pet.add_stars(0, message)
    await message.delete()


# Хэндлер для отметки прогулок
@tracking_router.message(Command(commands='walking'))
async def process_walking_command(message: Message):
    thirty_button = InlineKeyboardButton(text='30 или более минут',
                                         callback_data='thirty')
    twenty_button = InlineKeyboardButton(text='менее 30 минут',
                                         callback_data='twenty')
    zero_button = InlineKeyboardButton(text='совсем сегодня не погуляли',
                                       callback_data='zero')
    keyboard: list[list[InlineKeyboardButton]] = [[zero_button, twenty_button],
                                                  [thirty_button]]
    markup = InlineKeyboardMarkup(inline_keyboard=keyboard)
    await message.answer(LEXICON['/walking'], reply_markup=markup)


@tracking_router.callback_query(lambda c: c.data in ['twenty', 'thirty',
                                                     'zero'])
async def process_walking_callback(callback_query: CallbackQuery):
    answer = callback_query.data
    message = callback_query.message
    if answer == 'thirty':
        await message.answer('Вы заработали 2 супер звездочки счастья')
        await pet.add_stars(2, message)
    elif answer == 'twenty':
        await message.answer('Вы заработали 1 супер звездочку счастья')
        await pet.add_stars(1, message)
    elif answer == 'zero':
        await message.answer('Вы не заработали ни одной звездочки счастья\n\n'
                             'Stupid waklks for ur mental health не зря так '
                             'популярны в интернетах, такие моменты помогают '
                             'заземлиться')
        await pet.add_stars(0, message)
    await message.delete()


# Хэндлер для отметки медитаций
@tracking_router.message(Command(commands='meditation'))
async def process_meditation_command(message: Message):
    ten_button = InlineKeyboardButton(text='10 или более минут',
                                      callback_data='ten')
    nine_button = InlineKeyboardButton(text='менее 10 минут',
                                       callback_data='nine')
    zero_button = InlineKeyboardButton(text='совсем не медитировали',
                                       callback_data='zero')
    keyboard: list[list[InlineKeyboardButton]] = [[zero_button, nine_button],
                                                  [ten_button]]
    markup = InlineKeyboardMarkup(inline_keyboard=keyboard)
    await message.answer(LEXICON['/meditation'], reply_markup=markup)


@tracking_router.callback_query(lambda c: c.data in ['ten', 'nine', 'zero'])
async def process_meditation_callback(callback_query: CallbackQuery):
    answer = callback_query.data
    message = callback_query.message
    if answer == 'ten':
        await message.answer('Вы заработали 2 супер звездочки счастья')
        await pet.add_stars(2, message)
    elif answer == 'nine':
        await message.answer('Вы заработали 1 супер звездочку счастья')
        await pet.add_stars(1, message)
    elif answer == 'zero':
        await message.answer('Вы не заработали ни одной звездочки счастья\n\n'
                             'Помни, медитация помогает успокоиться, '
                             'сконцентрироваться и продолжить жить жизнь')
        await pet.add_stars(0, message)
    await message.delete()


# Хэндлер для отметки ведения дневника
@tracking_router.message(Command(commands='diary'))
async def process_diary_command(message: Message):
    yes_button = InlineKeyboardButton(text='да, записали в дневничок все '
                                           'свои мысли',
                                      callback_data='yes')
    no_button = InlineKeyboardButton(text='нет, сегодня не получилось',
                                     callback_data='no')
    keyboard: list[list[InlineKeyboardButton]] = [[no_button],
                                                  [yes_button]]
    markup = InlineKeyboardMarkup(inline_keyboard=keyboard)
    await message.answer(LEXICON['/diary'], reply_markup=markup)


@tracking_router.callback_query(lambda c: c.data in ['yes', 'no'])
async def process_diary_callback(callback_query: CallbackQuery):
    answer = callback_query.data
    message = callback_query.message
    if answer == 'yes':
        await message.answer('Вы заработали 2 супер звездочки счастья')
        await pet.add_stars(2, message)
    elif answer == 'no':
        await message.answer('Вы не заработали ни одной звездочки счастья\n\n'
                             'Дневничок - это прекрасный способ хранить '
                             'все свои самые мудрые идеи и рефлексировать '
                             'поведение себя из прошлого')
        await pet.add_stars(0, message)
    await message.delete()
