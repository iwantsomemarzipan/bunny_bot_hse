from aiogram import Router
from aiogram.filters import Command
from aiogram.types import (Message, InlineKeyboardButton,
                           InlineKeyboardMarkup)
from lexicon.lexicon import LEXICON

help_router: Router = Router()

url_button_1: InlineKeyboardButton = InlineKeyboardButton(
    text='Инструкция',
    url='https://www.notion.so/290d07905fdf4f8e999e55f9d86add4e')
url_button_2: InlineKeyboardButton = InlineKeyboardButton(
    text='Скины кролика',
    url='https://grandiose-carnation-fd4.notion.site/a4da98f1d5894316b9e8e264ebac23e1')

keyboard: InlineKeyboardMarkup = InlineKeyboardMarkup(
    inline_keyboard=[[url_button_1],
                     [url_button_2]])


@help_router.message(Command(commands='help'))
async def process_help_command(message: Message):
    await message.answer(LEXICON['/help'], reply_markup=keyboard)
