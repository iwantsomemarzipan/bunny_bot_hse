from aiogram.types import InlineKeyboardButton

pink_button = InlineKeyboardButton(text='1.1',
                                   callback_data='pink')
yellow_button = InlineKeyboardButton(text='2.1',
                                     callback_data='yellow')
purple_button = InlineKeyboardButton(text='3.1',
                                     callback_data='purple')
blue_button = InlineKeyboardButton(text='4.1',
                                   callback_data='blue')
lying_pink_button = InlineKeyboardButton(text='1.2',
                                         callback_data='lying_pink')
lying_yellow_button = InlineKeyboardButton(text='2.2',
                                           callback_data='lying_yellow')
lying_purple_button = InlineKeyboardButton(text='3.2',
                                           callback_data='lying_purple')
lying_blue_button = InlineKeyboardButton(text='4.2',
                                         callback_data='lying_blue')
pink_ears_button = InlineKeyboardButton(text='1.3',
                                        callback_data='pink_ears')
yellow_ears_button = InlineKeyboardButton(text='2.3',
                                          callback_data='yellow_ears')
purple_ears_button = InlineKeyboardButton(text='3.3',
                                          callback_data='purple_ears')
blue_ears_button = InlineKeyboardButton(text='4.3',
                                        callback_data='blue_ears')
pink_flower_button = InlineKeyboardButton(text='1.1.1',
                                          callback_data='pink_flower')
yellow_flower_button = InlineKeyboardButton(text='2.1.1',
                                            callback_data='yellow_flower')
purple_flower_button = InlineKeyboardButton(text='3.1.1',
                                            callback_data='purple_flower')
blue_flower_button = InlineKeyboardButton(
    text='4.1.1',
    callback_data='blue_flower'
    )
lying_pink_flower_button = InlineKeyboardButton(
    text='1.2.1',
    callback_data='lying_pink_flower'
    )
lying_yellow_flower_button = InlineKeyboardButton(
    text='2.2.1',
    callback_data='lying_yellow_flower'
    )
lying_purple_flower_button = InlineKeyboardButton(
    text='3.2.1',
    callback_data='lying_purple_flower'
    )
lying_blue_flower_button = InlineKeyboardButton(
    text='4.2.1',
    callback_data='lying_blue_flower'
    )
pink_ears_flower_button = InlineKeyboardButton(
    text='1.3.1',
    callback_data='pink_years_flower'
    )
yellow_ears_flower_button = InlineKeyboardButton(
    text='2.3.1',
    callback_data='yellow_ears_flower'
    )
purple_ears_flower_button = InlineKeyboardButton(
    text='3.3.1',
    callback_data='purple_ears_flower'
    )
blue_ears_flower_button = InlineKeyboardButton(text='4.3.1',
                                               callback_data='blue_ears')
pink_lamp_button = InlineKeyboardButton(text='1.1.2',
                                        callback_data='pink_lamp')
yellow_lamp_button = InlineKeyboardButton(text='2.1.2',
                                          callback_data='yellow_lamp')
purple_lamp_button = InlineKeyboardButton(text='3.1.2',
                                          callback_data='purple_lamp')
blue_lamp_button = InlineKeyboardButton(text='4.1.2',
                                        callback_data='blue_lamp')
lying_pink_lamp_button = InlineKeyboardButton(text='1.2.2',
                                              callback_data='lying_pink_lamp')
lying_yellow_lamp_button = InlineKeyboardButton(
    text='2.2.2',
    callback_data='lying_yellow_lamp'
    )
lying_purple_lamp_button = InlineKeyboardButton(
    text='3.2.2',
    callback_data='lying_purple_lamp'
    )
lying_blue_lamp_button = InlineKeyboardButton(text='4.2.2',
                                              callback_data='lying_blue_lamp')
pink_ears_lamp_button = InlineKeyboardButton(text='1.3.2',
                                             callback_data='pink_ears_lamp')
yellow_ears_lamp_button = InlineKeyboardButton(
    text='2.3.2',
    callback_data='yellow_ears_lamp'
    )
purple_ears_lamp_button = InlineKeyboardButton(
    text='3.3.2',
    callback_data='purple_ears_lamp'
    )
blue_ears_lamp_button = InlineKeyboardButton(text='4.3.2',
                                             callback_data='blue_ears_lamp')
pink_candle_button = InlineKeyboardButton(text='1.1.3',
                                          callback_data='pink_candle')
yellow_candle_button = InlineKeyboardButton(text='2.1.3',
                                            callback_data='yellow_candle')
purple_candle_button = InlineKeyboardButton(text='3.1.3',
                                            callback_data='purple_candle')
blue_candle_button = InlineKeyboardButton(text='4.1.3',
                                          callback_data='blue_candle')
lying_pink_candle_button = InlineKeyboardButton(
    text='1.2.3',
    callback_data='lying_pink_candle'
    )
lying_yellow_candle_button = InlineKeyboardButton(
    text='2.2.3',
    callback_data='lying_yellow_candle'
    )
lying_purple_candle_button = InlineKeyboardButton(
    text='3.2.3',
    callback_data='lying_purple_candle'
    )
lying_blue_candle_button = InlineKeyboardButton(
    text='4.2.3',
    callback_data='lying_blue_candle'
    )
pink_ears_candle_button = InlineKeyboardButton(
    text='1.3.3',
    callback_data='pink_ears_candle'
    )
yellow_ears_candle_button = InlineKeyboardButton(
    text='2.3.3',
    callback_data='yellow_ears_candle'
    )
purple_ears_candle_button = InlineKeyboardButton(
    text='3.3.3',
    callback_data='purple_ears_candle'
    )
blue_ears_candle_button = InlineKeyboardButton(
    text='4.3.3',
    callback_data='blue_ears_candle'
    )
