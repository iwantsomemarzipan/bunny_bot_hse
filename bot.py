import asyncio
import logging

from config_reader import config
from aiogram import Bot, Dispatcher
from handlers.user_handlers import user_router
from handlers.tracking_handlers import tracking_router
from handlers.help_handler import help_router
from aiogram.fsm.storage.memory import MemoryStorage

logging.basicConfig(level=logging.INFO)

storage: MemoryStorage = MemoryStorage()
bot: Bot = Bot(token=config.bot_token.get_secret_value())
dp: Dispatcher = Dispatcher()

dp.include_router(user_router)
dp.include_router(tracking_router)
dp.include_router(help_router)


async def main():
    await dp.start_polling(bot)

if __name__ == '__main__':
    asyncio.run(main())
