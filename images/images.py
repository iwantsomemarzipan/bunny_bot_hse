from aiogram.types import URLInputFile

# x.1
pink = URLInputFile(
    'https://i.pinimg.com/originals/ac/64/37/ac64379ed60505eb403b53e85ac84985.jpg',
    filename='pink.jpeg'
    )
yellow = URLInputFile(
    'https://i.pinimg.com/originals/ba/4a/be/ba4abe3891210ab9a322491f0bba2ded.jpg',
    filename='yellow.jpeg'
    )
purple = URLInputFile(
    'https://i.pinimg.com/originals/aa/ac/23/aaac23b21b788ab501a1f9eb9556c9d9.jpg',
    filename='purple.jpeg'
    )
blue = URLInputFile(
    'https://i.pinimg.com/originals/37/a1/cc/37a1ccdc468a0a5d48f9f0934dbcd35e.jpg',
    filename='blue.jpeg'
    )

# x.2
lying_pink = URLInputFile(
    'https://i.pinimg.com/originals/a2/95/a5/a295a5d14a2790471b052aa6466ef9e3.jpg',
    filename='lying_pink.jpeg'
    )
lying_yellow = URLInputFile(
    'https://i.pinimg.com/originals/d1/ca/35/d1ca358cf974cad4274fd0ff5d4ddcc4.jpg',
    filename='lying_yellow.jpeg'
    )
lying_purple = URLInputFile(
    'https://i.pinimg.com/originals/de/7d/a5/de7da5c5908f0bbc80f87326169ac6fd.jpg',
    filename='lying_purple.jpeg'
    )
lying_blue = URLInputFile(
    'https://i.pinimg.com/originals/80/2d/28/802d28f29328bd4ade78b4d681b5a39b.jpg',
    filename='lying_blue.jpeg'
    )

# x.3
pink_ears = URLInputFile(
    'https://i.pinimg.com/originals/18/4e/2e/184e2e2f696cc17c348de3be049da296.jpg',
    filename='pink_ears.jpeg'
    )
yellow_ears = URLInputFile(
    'https://i.pinimg.com/originals/b7/19/89/b7198916b9c8d77563dabf9ca6be8254.jpg',
    filename='yellow_ears.jpeg'
    )
purple_ears = URLInputFile(
    'https://i.pinimg.com/originals/e1/aa/34/e1aa34ce9bec6c15c55f50a43d41fe1e.jpg',
    filename='purple_ears.jpeg'
    )
blue_ears = URLInputFile(
    'https://i.pinimg.com/originals/8a/ec/54/8aec54ebbc5d452b495f1941430c5f8d.jpg',
    filename='blue_ears.jpeg'
    )

# 1.1.x
pink_flower = URLInputFile(
    'https://i.pinimg.com/originals/1e/58/b8/1e58b8ccf546fce0a9f1361da9b81034.jpg',
    filename='pink_flower.jpeg'
    )
pink_lamp = URLInputFile(
    'https://i.pinimg.com/originals/cc/bf/5d/ccbf5dc4117586815dd5a508b7036721.jpg',
    filename='pink_lamp.jpeg'
    )
pink_candle = URLInputFile(
    'https://i.pinimg.com/originals/1e/7a/b8/1e7ab8dd527487a087ef7fb2ac117a73.jpg',
    filename='pink_candle.jpeg'
    )

# 2.1.x
yellow_flower = URLInputFile(
    'https://i.pinimg.com/originals/59/13/42/591342b71361292491b06cf1b2a16725.jpg',
    filename='yellow_flower.jpeg'
    )
yellow_lamp = URLInputFile(
    'https://i.pinimg.com/originals/6a/a3/74/6aa374fc01e09e11dc328ba19e6ff3b5.jpg',
    filename='yellow_lamp.jpeg'
    )
yellow_candle = URLInputFile(
    'https://i.pinimg.com/originals/1f/ad/20/1fad2052bb7d72e5614438f8060de19c.jpg',
    filename='yellow_candle.jpeg'
    )

# 3.1.x
purple_flower = URLInputFile(
    'https://i.pinimg.com/originals/29/86/48/29864879566fd566203f9aaba4c0f74a.jpg',
    filename='purple_flower.jpeg'
    )
purple_lamp = URLInputFile(
    'https://i.pinimg.com/originals/2b/15/42/2b154258fbe1d0dd7fb963c24dfa4e4a.jpg',
    filename='purple_lamp.jpeg'
    )
purple_candle = URLInputFile(
    'https://i.pinimg.com/originals/f1/63/b3/f163b3404de5fc5d8f20df017ac1de30.jpg',
    filename='purple_candle.jpeg'
    )

# 4.1.x
blue_flower = URLInputFile(
    'https://i.pinimg.com/originals/4b/26/8e/4b268ef59ac3dd2abe2a82fb41e27e42.jpg',
    filename='blue_flower.jpeg'
    )
blue_lamp = URLInputFile(
    'https://i.pinimg.com/originals/fa/fe/3e/fafe3e2e879a36944bd7c80282121cfc.jpg',
    filename='blue_lamp.jpeg'
    )
blue_candle = URLInputFile(
    'https://i.pinimg.com/originals/00/79/b7/0079b794f1372a8a376958b6f2679d7e.jpg',
    filename='blue_candle.jpeg'
    )

# 1.2.x
lying_pink_flower = URLInputFile(
    'https://i.pinimg.com/originals/63/68/07/63680748aa74d44e864b3457fdda54ef.jpg',
    filename='lying_pink_flower.jpeg'
    )
lying_pink_lamp = URLInputFile(
    'https://i.pinimg.com/originals/40/b2/53/40b2533b333773e8671914871b099f40.jpg',
    filename='lying_pink_lamp.jpeg'
    )
lying_pink_candle = URLInputFile(
    'https://i.pinimg.com/originals/63/95/d6/6395d6f835a6b55231b599844c4b3ca2.jpg',
    filename='lying_pink_candle.jpeg'
    )

# 2.2.x
lying_yellow_flower = URLInputFile(
    'https://i.pinimg.com/originals/8d/50/d7/8d50d74e95b1174078d5b49d40ccc830.jpg',
    filename='lying_yellow_flower.jpeg'
    )
lying_yellow_lamp = URLInputFile(
    'https://i.pinimg.com/originals/58/ac/45/58ac455f96e1723d4ee981fb79f4304f.jpg',
    filename='lying_yellow_lamp.jpeg'
    )
lying_yellow_candle = URLInputFile(
    'https://i.pinimg.com/originals/13/8f/66/138f66fcad6f3e35efb1c5eac3ec4683.jpg',
    filename='lying_yellow_candle.jpeg'
    )

# 3.2.x
lying_purple_flower = URLInputFile(
    'https://i.pinimg.com/originals/55/88/a6/5588a6eb3c7bd89170097f6599baa3b4.jpg',
    filename='lying_purple_flower.jpeg'
    )
lying_purple_lamp = URLInputFile(
    'https://i.pinimg.com/originals/67/85/68/67856834940d04d99733adde975d2801.jpg',
    filename='lying_purple_lamp.jpeg'
    )
lying_purple_candle = URLInputFile(
    'https://i.pinimg.com/originals/ef/fb/c7/effbc7d52da888739afff082d058274a.jpg',
    filename='lying_purple_candle.jpeg'
    )

# 4.2.x
lying_blue_flower = URLInputFile(
    'https://i.pinimg.com/originals/86/0b/3c/860b3cda304cf209d664dae0fd9be3f6.jpg',
    filename='lying_blue_flower.jpeg'
    )
lying_blue_lamp = URLInputFile(
    'https://i.pinimg.com/originals/1d/d2/db/1dd2dbdfcc23f71ff870381dfe37ff6c.jpg',
    filename='lying_blue_lamp.jpeg'
    )
lying_blue_candle = URLInputFile(
    'https://i.pinimg.com/originals/54/fa/d9/54fad93e43d058e951b8feee2c5b6ee9.jpg',
    filename='lying_blue_candle.jpeg'
    )

# 1.3.x
pink_ears_flower = URLInputFile(
    'https://i.pinimg.com/originals/2d/b5/14/2db514f502b9bee38eb6d870f7df6bec.jpg',
    filename='pink_flower.jpeg'
    )
pink_ears_lamp = URLInputFile(
    'https://i.pinimg.com/originals/5e/ce/22/5ece225988354cafb70ec0846c3b5e84.jpg',
    filename='pink_lamp.jpeg'
    )
pink_ears_candle = URLInputFile(
    'https://i.pinimg.com/originals/6a/1d/6b/6a1d6be95d84f1540b08a4e220c6703f.jpg',
    filename='pink_candle.jpeg'
    )

# 2.3.x
yellow_ears_flower = URLInputFile(
    'https://i.pinimg.com/originals/76/ea/d7/76ead71a31cce6915cfa32cee9ad0fc3.jpg',
    filename='yellow_ears_flower.jpeg'
    )
yellow_ears_lamp = URLInputFile(
    'https://i.pinimg.com/originals/d8/06/b9/d806b976327af002ba0b7c80b78391b6.jpg',
    filename='yellow_ears_lamp.jpeg'
    )
yellow_ears_candle = URLInputFile(
    'https://i.pinimg.com/originals/50/13/cc/5013ccf2716ba43c3ae76a3c3b22adc0.jpg',
    filename='yellow_ears_candle.jpeg'
    )

# 3.3.x
purple_ears_flower = URLInputFile(
    'https://i.pinimg.com/originals/35/be/4f/35be4f55257c38d2649aed0289aa5f1e.jpg',
    filename='purple_ears_flower.jpeg'
    )
purple_ears_lamp = URLInputFile(
    'https://i.pinimg.com/originals/1f/d5/04/1fd50474cc70896dc4bdb3d9d693d76f.jpg',
    filename='purple_ears_lamp.jpeg'
    )
purple_ears_candle = URLInputFile(
    'https://i.pinimg.com/originals/99/2a/59/992a5992a0c9d3bdab4babdb80e48c3d.jpg',
    filename='purple_ears_candle.jpeg'
    )

# 4.3.x
blue_ears_flower = URLInputFile(
    'https://i.pinimg.com/originals/11/1b/b3/111bb3b3fd01ab2d16bd6f28151b9157.jpg',
    filename='blue_ears_flower.jpeg'
    )
blue_ears_lamp = URLInputFile(
    'https://i.pinimg.com/originals/e9/26/17/e92617f032a7de65c09b120ada186b58.jpg',
    filename='blue_ears_lamp.jpeg'
    )
blue_ears_candle = URLInputFile(
    'https://i.pinimg.com/originals/2f/a4/c6/2fa4c6cd5dab4984924a26c8f5185b65.jpg',
    filename='blue_ears_candle.jpeg'
    )
